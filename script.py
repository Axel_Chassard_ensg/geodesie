#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 08:57:09 2020

@author: wellan
"""

import numpy as np
import matplotlib as plt
from pyproj import Transformer
from partie2 import *



def transfo_euro(coord_etrf2014):
    '''
    '''
    
    transformer = Transformer.from_crs(8401,3035)

    return transformer.transform(xx=coord_etrf2014[0],yy=coord_etrf2014[1],zz=coord_etrf2014[2])
    
    #faire la commande avec from_pipeline pour écrire une commande proj

def transfo_itrf2005_etrf2014(coord_itrf2005):
    '''
    '''
    
    transformer = Transformer.from_crs(4896,8401)
    
    
#    for p in transformer.itransform(coord_itrf2005) :
#        coord_etrf2014 = p
    return transformer.transform(xx=coord_itrf2005[0][0],yy=coord_itrf2005[0][1],zz=coord_itrf2005[0][2])

def transfo_itrf2008_etrf2014(coord_itrf2008):
    '''
    '''
    
    transformer = Transformer.from_crs(5332,8401)
    
    for p in transformer.itransform(coord_itrf2008) :
        coord_etrf2014 = p
    return p


def propa(t,t0,x,vx):
    '''
    '''
    
    return x+(t-t0)*vx

def ecart_etrf2014(coord_calcule,coord_publie):
    '''
    '''
    
    return np.abs(coord_calcule-coord_publie)



if __name__ == "__main__" :


    #Question I.4. et I.6.
    
    #On Commence par convertir les données récupérées avec le calcul en ligne en ETRF2014
        
    #ACOR 2010 en ITRF2005, on le convertit en ETRF2014
    ACOR_2010_ligne_ITRF2005 = [(4594489.648, -678367.658, 4357066.155)]
    ACOR_2010_ligne_ETRF2014 = np.array(transfo_itrf2005_etrf2014(ACOR_2010_ligne_ITRF2005))
    
    #On récupère la valeur publié sur la page http://etrs89.ensg.ign.fr/pub/ssc/ETRF2014.SSC2
    ACOR_publie_xyz = np.array([4594489.8418,-678368.0607,4357065.9296])
    ACOR_publie_vxvyvz = np.array([-.00128,0.00384,-.00173])
    #On va calculer ces valeurs au 12 Janvier 2010
    ACOR_2010_publie_ETRF2014 = propa(2010+12/365,2010,ACOR_publie_xyz,ACOR_publie_vxvyvz)
    
    #On déduit les écarts entre les coordonnées calculées et les coordonnées de la publication
    ACOR_ecart_2010 = ecart_etrf2014(ACOR_2010_ligne_ETRF2014,ACOR_2010_publie_ETRF2014)
    
    #On projette les coordonnées calculées sur une couverture prenant l'Europe
    ACOR_euro_2010 = np.array(transfo_euro(ACOR_2010_ligne_ETRF2014))
    
    ACOR_publie_vxvyvz_new_2010 = mise_a_zero_vh(ACOR_2010_publie_ETRF2014, ACOR_publie_vxvyvz)
    
    # np.savetxt("./resultats/ACOR/ACOR_2010_ligne_ETRF2014.txt",ACOR_2010_ligne_ETRF2014,header="Résultats de la conversion du calcul en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/ACOR/ACOR_2010_publie_ETRF2014.txt",ACOR_2010_publie_ETRF2014,header="Résultats de la conversion du résultat publie en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/ACOR/Ecart_ACOR_2010_ETRF2014.txt",ACOR_ecart_2010,header="Ecarts entre le calcul en ligne et le résultat publié en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/ACOR/ACOR_euro_2010_ligne_ETRF2014.txt",ACOR_euro_2010,header="Résultats de la conversion du calcul en ligne en dans la projection européenne en 2010")
    # np.savetxt("./resultats/ACOR/ACOR_2010_publie_ETRF2014_vitesse_new.txt",ACOR_publie_vxvyvz_new_2010,header="Résultats de la nouvelle vitesse après mise à zéro de la compsante verticale dans le repère local 2010")
    
    
    #On réalise la même démarche pour toutes les autres coordonnées 
    
    #ACOR 2020
    
    ACOR_2020_ligne_ITRF2008 = [(4594489.550, -678367.436, 4357066.252)]
    ACOR_2020_ligne_ETRF2014 = transfo_itrf2008_etrf2014(ACOR_2020_ligne_ITRF2008)
    
    ACOR_2020_publie_ETRF2014 = propa(2020+12/365,2010,ACOR_publie_xyz,ACOR_publie_vxvyvz)
    
    ACOR_ecart_2020 = ecart_etrf2014(ACOR_2020_ligne_ETRF2014,ACOR_2020_publie_ETRF2014)
    
    ACOR_euro_2020 = np.array(transfo_euro(ACOR_2020_ligne_ETRF2014))
    
    ACOR_publie_vxvyvz_new_2020 = mise_a_zero_vh(ACOR_2010_publie_ETRF2014, ACOR_publie_vxvyvz)
    
    # np.savetxt("./resultats/ACOR/ACOR_2020_ligne_ETRF2014.txt",ACOR_2020_ligne_ETRF2014,header="Résultats de la conversion du calcul en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/ACOR/ACOR_2020_publie_ETRF2014.txt",ACOR_2020_publie_ETRF2014,header="Résultats de la conversion du résultat publie en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/ACOR/Ecart_ACOR_2020_ETRF2014.txt",ACOR_ecart_2020,header="Ecarts entre le calcul en ligne et le résultat publié en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/ACOR/ACOR_euro_2020_ligne_ETRF2014.txt",ACOR_euro_2020,header="Résultats de la conversion du calcul en ligne en dans la projection européenne en 2020")
    # np.savetxt("./resultats/ACOR/ACOR_2020_publie_ETRF2014_vitesse_new.txt",ACOR_publie_vxvyvz_new_2020,header="Résultats de la nouvelle vitesse après mise à zéro de la compsante verticale dans le repère local 2020")
    
    #FOYL 2010
    FOYL_2010_ligne_ITRF2005 = [(3638083.212, -468414.578, 5200402.532)]
    FOYL_2010_ligne_ETRF2014 = transfo_itrf2005_etrf2014(FOYL_2010_ligne_ITRF2005)
    
    FOYL_publie_xyz = np.array([3638083.4486,-468414.9045,5200402.3700])
    FOYL_publie_vxvyvz = np.array([0.00031,-.00004,0.00117])
    
    FOYL_2010_publie_ETRF2014 = propa(2010+12/365,2010,FOYL_publie_xyz,FOYL_publie_vxvyvz)
    
    FOYL_ecart_2010 = ecart_etrf2014(FOYL_2010_ligne_ETRF2014,FOYL_2010_publie_ETRF2014)
    
    FOYL_euro_2010 = np.array(transfo_euro(FOYL_2010_ligne_ETRF2014))
    
    FOYL_publie_vxvyvz_new_2010 = mise_a_zero_vh(FOYL_2010_publie_ETRF2014, FOYL_publie_vxvyvz)
     
    # np.savetxt("./resultats/FOYL/FOYL_2010_ligne_ETRF2014.txt",FOYL_2010_ligne_ETRF2014,header="Résultats de la conversion du calcul en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/FOYL/FOYL_2010_publie_ETRF2014.txt",FOYL_2010_publie_ETRF2014,header="Résultats de la conversion du résultat publie en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/FOYL/Ecart_FOYL_2010_ETRF2014.txt",FOYL_ecart_2010,header="Ecarts entre le calcul en ligne et le résultat publié en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/FOYL/FOYL_euro_2010_ligne_ETRF2014.txt",FOYL_euro_2010,header="Résultats de la conversion du calcul en ligne en dans la projection européenne en 2010")
    # np.savetxt("./resultats/FOYL/FOYL_2020_publie_ETRF2014_vitesse_new.txt",FOYL_publie_vxvyvz_new_2010,header="Résultats de la nouvelle vitesse après mise à zéro de la compsante verticale dans le repère local 2010")
   
    
    #FOYL 2020
    
    FOYL_2020_ligne_ITRF2008 = [(3638083.073, -468414.424, 5200402.609)]
    FOYL_2020_ligne_ETRF2014 = transfo_itrf2008_etrf2014(FOYL_2020_ligne_ITRF2008)
    
    FOYL_2020_publie_ETRF2014 = propa(2020+12/365,2010,FOYL_publie_xyz,FOYL_publie_vxvyvz)
     
    FOYL_ecart_2020 = ecart_etrf2014(FOYL_2020_ligne_ETRF2014,FOYL_2020_publie_ETRF2014)
    
    FOYL_euro_2020 = np.array(transfo_euro(FOYL_2020_ligne_ETRF2014))
    
    FOYL_publie_vxvyvz_new_2020 = mise_a_zero_vh(FOYL_2010_publie_ETRF2014, FOYL_publie_vxvyvz)
    
    # np.savetxt("./resultats/FOYL/FOYL_2020_ligne_ETRF2014.txt",FOYL_2020_ligne_ETRF2014,header="Résultats de la conversion du calcul en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/FOYL/FOYL_2020_publie_ETRF2014.txt",FOYL_2020_publie_ETRF2014,header="Résultats de la conversion du résultat publie en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/FOYL/Ecart_FOYL_2020_ETRF2014.txt",FOYL_ecart_2020,header="Ecarts entre le calcul en ligne et le résultat publié en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/FOYL/FOYL_euro_2020_ligne_ETRF2014.txt",FOYL_euro_2020,header="Résultats de la conversion du calcul en ligne en dans la projection européenne en 2020")
    # np.savetxt("./resultats/FOYL/FOYL_2020_publie_ETRF2014_vitesse_new.txt",FOYL_publie_vxvyvz_new_2020,header="Résultats de la nouvelle vitesse après mise à zéro de la compsante verticale dans le repère local 2020")
   
    
    #VACO 2010
    
    VACO_2010_ligne_ITRF2005 = [(4062325.827, 992104.734, 4800911.323)]
    VACO_2010_ligne_ETRF2014 = transfo_itrf2005_etrf2014(VACO_2010_ligne_ITRF2005)   
    
    VACO_publie_xyz = np.array([4062326.1401,992104.3579,4800911.1158])
    VACO_publie_vxvyvz = np.array([-.00034,-.00005,-.00063])
    
    VACO_2010_publie_ETRF2014 = propa(2010+12/365,2010,VACO_publie_xyz,VACO_publie_vxvyvz)
    
    VACO_ecart_2010 = ecart_etrf2014(VACO_2010_ligne_ETRF2014,VACO_2010_publie_ETRF2014)
    
    VACO_euro_2010 = np.array(transfo_euro(VACO_2010_ligne_ETRF2014))
     
    VACO_publie_vxvyvz_new_2010 = mise_a_zero_vh(VACO_2010_publie_ETRF2014, VACO_publie_vxvyvz)
    
    # np.savetxt("./resultats/VACO/VACO_2010_ligne_ETRF2014.txt",VACO_2010_ligne_ETRF2014,header="Résultats de la conversion du calcul en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/VACO/VACO_2010_publie_ETRF2014.txt",VACO_2010_publie_ETRF2014,header="Résultats de la conversion du résultat publie en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/VACO/Ecart_VACO_2010_ETRF2014.txt",VACO_ecart_2010,header="Ecarts entre le calcul en ligne et le résultat publié en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/VACO/VACO_euro_2010_ligne_ETRF2014.txt",VACO_euro_2010,header="Résultats de la conversion du calcul en ligne en dans la projection européenne en 2010")
    # np.savetxt("./resultats/VACO/VACO_2010_publie_ETRF2014_vitesse_new.txt",VACO_publie_vxvyvz_new_2010,header="Résultats de la nouvelle vitesse après mise à zéro de la compsante verticale dans le repère local 2010")
   
    
    #VACO 2020
    
    VACO_2020_ligne_ITRF2008 = [(4062325.633, 992104.879, 4800911.382)]
    VACO_2020_ligne_ETRF2014 = transfo_itrf2008_etrf2014(VACO_2020_ligne_ITRF2008)
    
    VACO_2020_publie_ETRF2014 = propa(2020+12/365,2010,VACO_publie_xyz,VACO_publie_vxvyvz)
    
    VACO_ecart_2020 = ecart_etrf2014(VACO_2020_ligne_ETRF2014,VACO_2020_publie_ETRF2014)
    
    VACO_euro_2020 = np.array(transfo_euro(VACO_2020_ligne_ETRF2014))
    
    VACO_publie_vxvyvz_new_2020 = mise_a_zero_vh(VACO_2010_publie_ETRF2014, VACO_publie_vxvyvz)
    
    # np.savetxt("./resultats/VACO/VACO_2020_ligne_ETRF2014.txt",VACO_2020_ligne_ETRF2014,header="Résultats de la conversion du calcul en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/VACO/VACO_2020_publie_ETRF2014.txt",VACO_2020_publie_ETRF2014,header="Résultats de la conversion du résultat publie en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/VACO/Ecart_VACO_2020_ETRF2014.txt",VACO_ecart_2020,header="Ecarts entre le calcul en ligne et le résultat publié en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/VACO/VACO_euro_2020_ligne_ETRF2014.txt",VACO_euro_2020,header="Résultats de la conversion du calcul en ligne en dans la projection européenne en 2020")
    # np.savetxt("./resultats/VACO/VACO_2020_publie_ETRF2014_vitesse_new.txt",VACO_publie_vxvyvz_new_2020,header="Résultats de la nouvelle vitesse après mise à zéro de la compsante verticale dans le repère local 2020")
   
    
    
    #VISO 2010
    
    VISO_2010_ligne_ITRF2005 = [(3246470.177, 1077900.587, 5365278.125)]
    VISO_2010_ligne_ETRF2014 = transfo_itrf2005_etrf2014(VISO_2010_ligne_ITRF2005)
    
    #Les informations de la station VISO ne sont pas sur la page http://etrs89.ensg.ign.fr/pub/ssc/ETRF2014.SSC2
    
    VISO_euro_2010 = np.array(transfo_euro(VISO_2010_ligne_ETRF2014))
    
    # np.savetxt("./resultats/VISO/VISO_2010_ligne_ETRF2014.txt",VISO_2010_ligne_ETRF2014,header="Résultats de la conversion du calcul en ligne en ETRF2014 en 2010")
    # np.savetxt("./resultats/VISO/VISO_euro_2010_ligne_ETRF2014.txt",VACO_euro_2010,header="Résultats de la conversion du calcul en ligne en dans la projection européenne en 2010")
    
    
    #VISO 2020
    
    VISO_2020_ligne_ITRF2008 = [(3246470.012, 1077900.727, 5365278.210)]
    VISO_2020_ligne_ETRF2014 = transfo_itrf2008_etrf2014(VISO_2020_ligne_ITRF2008)
    
    VISO_euro_2020 = np.array(transfo_euro(VISO_2020_ligne_ETRF2014))
    
    # np.savetxt("./resultats/VISO/VISO_2020_ligne_ETRF2014.txt",VISO_2020_ligne_ETRF2014,header="Résultats de la conversion du calcul en ligne en ETRF2014 en 2020")
    # np.savetxt("./resultats/VISO/VISO_euro_2020_ligne_ETRF2014.txt",VACO_euro_2020,header="Résultats de la conversion du calcul en ligne en dans la projection européenne en 2020")
    
    
    
    obs_2010 = np.vstack((creation_sous_mat_obs(ACOR_2010_publie_ETRF2014),creation_sous_mat_obs(FOYL_2010_publie_ETRF2014),creation_sous_mat_obs(VACO_2010_publie_ETRF2014)))
    vitesse_2010 = np.vstack((np.reshape(ACOR_publie_vxvyvz_new_2010,(-1,1)),np.reshape(FOYL_publie_vxvyvz_new_2010,(-1,1)),np.reshape(VACO_publie_vxvyvz_new_2010,(-1,1))))
   
    X_2010 = moindre_carres_lineaire_omega(obs_2010,vitesse_2010)
    # np.savetxt("./resultats/Omega_2010.txt",X_2010,header="Résulats des moindres carrés linéaires pour la determination de l'axe du pôle")
   
    
    obs_2020 = np.vstack((creation_sous_mat_obs(ACOR_2020_publie_ETRF2014),creation_sous_mat_obs(FOYL_2020_publie_ETRF2014),creation_sous_mat_obs(VACO_2020_publie_ETRF2014)))
    vitesse_2020 = np.vstack((np.reshape(ACOR_publie_vxvyvz_new_2020,(-1,1)),np.reshape(FOYL_publie_vxvyvz_new_2020,(-1,1)),np.reshape(VACO_publie_vxvyvz_new_2020,(-1,1))))
   
    X_2020 = moindre_carres_lineaire_omega(obs_2020,vitesse_2020)
    
    # np.savetxt("./resultats/Omega_2020.txt",X_2020,header="Résulats des moindres carrés linéaires pour la determination de l'axe du pôle")
   
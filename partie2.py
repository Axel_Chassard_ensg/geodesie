# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 14:58:27 2020

@author: Alex
"""

import numpy as np
from pyproj import Transformer


def vitesse_glob_to_vitesse_topo_locale(v_glob, lamb, phi):
    """
    Fonction permettant de calculer la vitesse topocentrique locale à 
    partit d'une vitesse globale

    Parameters
    ----------
    v_glob : TYPE [1 * 3]
        DESCRIPTION. vecteur de vitesse globales
    lamb : TYPE float
        DESCRIPTION. angle en radian
    phi : TYPE float
        DESCRIPTION. angle en radian

    Returns 
    -------
    v_topo : TYPE [1 * 3]
        DESCRIPTION. vecteur de vitesses topocentriques locales

    """
    


    r = np.array([[-np.sin(lamb),np.cos(lamb),0],[-np.sin(phi)*np.cos(lamb),-np.sin(phi)*np.sin(lamb),np.cos(phi)],[np.cos(phi)*np.cos(lamb),np.cos(phi)*np.sin(lamb),np.sin(phi)]])
    
    v_topo = np.dot(r,v_glob.T)
    
    return v_topo
    

def vitesse_topo_locale_to_vitesse_glob(v_topo,lamb,phi):
    """
    Fonction permettant de calculer la vitesse glpbale à 
    partit d'une vitesse topocentrique locale
    
    Parameters
    ----------
    v_topo : TYPE [1 * 3]
        DESCRIPTION. vecteur de vitesse topocentrique local
    lamb : TYPE float
        DESCRIPTION. angle en radian
    phi : TYPE float
        DESCRIPTION. angle en radian

    Returns 
    -------
    v_glob : TYPE [1 * 3]
        DESCRIPTION. vecteur de vitesses globales

    """
    
    r = np.array([[-np.sin(lamb),np.cos(lamb),0],[-np.sin(phi)*np.cos(lamb),-np.sin(phi)*np.sin(lamb),np.cos(phi)],[np.cos(phi)*np.cos(lamb),np.cos(phi)*np.sin(lamb),np.sin(phi)]])
    
    v_glob = np.dot(np.linalg.pinv(r),v_topo)
    
    return v_glob


# def DMS2Rad(D, M, S):
#     """
#     Fonction ayant pour but de convertir des Degrés Minutes Secondes en Radian

#     Parameters
#     ----------
#     D : TYPE int
#         DESCRIPTION. degree
#     M : TYPE int
#         DESCRIPTION. minute
#     S : TYPE float
#         DESCRIPTION. seconde

#     Returns
#     -------
#     TYPE float
#         DESCRIPTION. angle en radian

#     """
#     return ((D+M/60+S/3600)/180*3.141592654)

    



        
def ETRF2014_cart_to_ETFR2014_geog(coord):
    """
    Fonction ayant pour but de passer de coordonnées ETRF2014 cartesiennes en coodonnees
    ETFR2014 geographiques

    Parameters
    ----------
    coord : TYPE np array [1*3]
        DESCRIPTION. coordonnees ETRF2014 cartésiennes

    Returns
    -------
    phi : TYPE float
        DESCRIPTION. latitude
    lamb : TYPE float
        DESCRIPTION. longitude
    h : TYPE float
        DESCRIPTIO. hauteur

    """
    transformer = Transformer.from_crs(8401,8403)
    phi,lamb,h=transformer.transform(xx=coord[0],yy=coord[1],zz=coord[2])
    
    return phi, lamb, h


def creation_sous_mat_obs(obs):
    
    sous_mat_obs = np.zeros((3,3))
    sous_mat_obs[0,1] , sous_mat_obs[1,0] = -obs[2] , obs[2]
    sous_mat_obs[0,2] , sous_mat_obs[2,0] = obs[1] , -obs[1]
    sous_mat_obs[1,2] , sous_mat_obs[2,1] = -obs[0] , obs[0]
    
    return sous_mat_obs


def mise_a_zero_vh(coord, vitesse):
    """
    Fonction ayant pour but de mettre à zero la composante verticale de la vitesse

    Parameters
    ----------
    coord : TYPE np array [1*3]
        DESCRIPTION. coordonnées ETRF2014 cartésiennes
    vitesse : TYPE np array [1*3]
        DESCRIPTION. vitesse associée au coordonnées cartésiennes

    Returns
    -------
    v_glob_new : TYPE np array [1*3]
        DESCRIPTION. vitesse en ETRF2014

    """
    
    phi, lamb, h = ETRF2014_cart_to_ETFR2014_geog(coord)
    v_loc = vitesse_glob_to_vitesse_topo_locale(coord,lamb,phi)
    #mise a zero de la vitesse verticale dans le repere topocentrique local
    v_loc[2] = 0
    v_glob_new = vitesse_topo_locale_to_vitesse_glob(v_loc,lamb,phi)   
    
    return v_glob_new


# def moindre_carres_lineaire_omega(obs,vitesse):
#     """
#     Fonction permettant de resoudre un probleme de moindres carre linéaires

#     Parameters
#     ----------
#     obs : TYPE np array [3*n]
#         DESCRIPTION. matrice des observations
#     vitesse : TYPE np array [3*n]
#         DESCRIPTION. matrice des vitesses

#     Returns
#     -------
#     X : TYPE np array [3*3]
#         DESCRIPTION. vecteur des paramètres

#     """
    
#     obs = np.reshape(obs, (3,-1))
#     vitesse = np.reshape(vitesse, (3,-1))

#     X = np.dot(vitesse, np.linalg.pinv(obs))    
    
#     return X

def moindre_carres_lineaire_omega(obs,vitesse):
    """
    Fonction permettant de resoudre un probleme de moindres carre linéaires

    Parameters
    ----------
    obs : TYPE np array [n*3]
        DESCRIPTION. matrice des observations
    vitesse : TYPE np array [n*1]
        DESCRIPTION. matrice des vitesses

    Returns
    -------
    X : TYPE np array [3*1]
        DESCRIPTION. vecteur des paramètres

    """
    
    obs = np.reshape(obs, (-1,3))
    vitesse = np.reshape(vitesse, (-1,1))

    X = np.dot(np.linalg.pinv(obs),vitesse)    
    
    return X    
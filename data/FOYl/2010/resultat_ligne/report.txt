*******************************************************************
FOYL_55208.00 PPP INITIALISATION     starting @ 2020 03 20 09:07:19
*******************************************************************
ppp_x version 2.3 2018 08 03
quick view on (uncompressed)file foyl0120.10d
mjd_start 55208.0000  mjd_end 55208.9997 (23.9917h) sampling 30.0000s
epochs # 2857 (limitation 200000) holes in sampled data 1
HOLE 1 :: ]11:58:30, 12:10:30[  (12.00 min)
(0.1445 sec)
*******************************************************************
FOYL_55208.00 PPP PREPARATION REPORT starting @ 2020 03 20 09:07:19
*******************************************************************
Orbits/clocks/erp for mjd 55208.00 OK (sgu15663_00.sp3/sgu15663_00.clk_30s/sgu15663_00.erp)
Astro elements for mjd 55208.00 OK
Orbits for mjd 55208.00 successfully completed
RSW RMS (m) :   0.0020  0.0071  0.0018
SVs 30 G02 G03 G04 G05 G06 G07 G08 G09 G10 G11 G12 G13 G14 G15 G16 G17 G18 G19 G20 G21 G22 G23 G24 G26 G27 G28 G29 G30 G31 G32
***warning*** large residuals for SV's : G15 G09 G18 G27 G22 G05 G10 G14 G26 G06 G19 G07 G03 G17 G02 G04 G11 G08
REFERENCE FRAME :: ITRF2005[IGS05]
(23.39 sec)
*******************************************************************
FOYL_55208.00 PPP COMPUTATION REPORT starting @ 2020 03 20 09:07:42
*******************************************************************
STATION NAME   : FOYL                                                        
STATION NUMBER : 13241M001                                                   
RECEIVER TYPE  : LEICA GRX1200GGPRO  
ANTENNA TYPE   : LEIAT504GG      LEIS
IGS08_1930 <LEIAT504GG      LEIS>     L1 : dN  0.00040m  dE  0.00117m  dU  0.08706m
IGS08_1930 <LEIAT504GG      LEIS>     L2 : dN  0.00014m  dE -0.00032m  dU  0.11754m
           <LEIAT504GG      LEIS>  IF(G) : dN  0.00080m  dE  0.00347m  dU  0.03995m
           <LEIAT504GG      LEIS>  IF(R) : dN  0.00080m  dE  0.00345m  dU  0.04039m
           <LEIAT504GG      LEIS>  IF(E) : dN  0.00077m  dE  0.00329m  dU  0.04372m
ANTENNA HEIGHT : 0.0000
APPROX POS RNX : 3638083.485 -468414.868 5200402.325  (W 007°20'11.94524"  N 54°59'01.51055"  h 68.8983)
DATE FIRST     : 2010 01 12 00 00 00.0
DATE LAST      : 2010 01 12 23 59 30.0
EPOCHS         : 2857
INTERV         : 30.000
MILLISECOND JUMPS NONE
SV  w/o EPH    : 16  R02 R03 R05 R07 R08 R09 R10 R11 R13 R14 R15 R17 R18 R19 R20 R21 
SV  used       : 30  G02 G03 G04 G05 G06 G07 G08 G09 G10 G11 G12 G13 G14 G15 G16 G17 G18 G19 G20 G21 G22 G23 G24 G26 G27 G28 G29 G30 G31 G32 
AMBIG.         : 88

APPROX SPP COMPUTATION (1ST EPOCH)
**********************************
APPROX POS SPP : 3638083.372 -468414.468 5200403.216

SPP COMPUTATION (t=1.197s)
***************************
MEAN INTVL (s) : 30.242
MEAN XYZ COORD (SPP)           3638083.200 -468414.503 5200402.451
MEAN geo COORD (SPP)           W 007°20'11.92691"  N 54°59'01.52161"  h 68.8134

APPROX EXTENT E x N x H m      0.6 x 1.1 x 2.7

PPP COMPUTATION (ITRF2005[IGS05] EP=2010.030)
*********************************************
ZTD relative constraint        0.010 m/h
ECLIPSE for sv G15 IIR-M   00:00:00 -> 00:34:30 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G12 IIR-M   00:51:00 -> 01:57:30 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G30 IIA     01:50:30 -> 02:53:30 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G16 IIR-A   05:11:00 -> 05:55:30 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G23 IIR-B   19:11:30 -> 19:31:30 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G28 IIR-A   21:10:00 -> 22:17:30 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G15 IIR-M   23:54:30 -> 23:59:30 (mismodelled wind-up :: deweighted)
NOON EVENTS   : NONE
actual epochs #                2857/2857
MEAN XYZ COORD (ITRF EPOCH)    3638083.212 -468414.578 5200402.532
MEAN geo COORD (ITRF EPOCH)    W 007°20'11.93101"  N 54°59'01.52254"  h 68.8920

MEAN XYZ with E_TIDES applied  3638083.222 -468414.578 5200402.569
MEAN geo with E_TIDES applied  W 007°20'11.93092"  N 54°59'01.52298"  h 68.9281

PPP EXTENT E x N x H m         0.536 x 0.474 x 0.505

PPP computation time = 3.713s

RESIDUALS STATISTICS
********************
VTPV 476965.578  nobs 52104 dof:37731  sig0 3.555
RMS SV G02   P 0.0068 C 0.1754   (nobs 963)
RMS SV G03   P 0.0118 C 0.0291   (nobs 696)
RMS SV G04   P 0.0058 C 0.3381   (nobs 892)
RMS SV G05   P 0.0064 C 0.2600   (nobs 895)
RMS SV G06   P 0.0087 C 0.0232   (nobs 706)
RMS SV G07   P 0.0185 C 0.2221   (nobs 1041)
RMS SV G08   P 0.0278 C 0.2705   (nobs 975)
RMS SV G09   P 0.0053 C 0.0945   (nobs 736)
RMS SV G10   P 0.0056 C 0.2568   (nobs 675)
RMS SV G11   P 0.0089 C 0.0531   (nobs 702)
RMS SV G12   P 0.0056 C 0.4551   (nobs 925)
RMS SV G13   P 0.0073 C 0.1975   (nobs 961)
RMS SV G14   P 0.0080 C 0.1979   (nobs 947)
RMS SV G15   P 0.0043 C 0.2042   (nobs 748)
RMS SV G16   P 0.0175 C 0.2172   (nobs 677)
RMS SV G17   P 0.0085 C 0.2310   (nobs 997)
RMS SV G18   P 0.0661 C 0.5297   (nobs 979)
RMS SV G19   P 0.0150 C 0.3975   (nobs 702)
RMS SV G20   P 0.0098 C 0.3013   (nobs 729)
RMS SV G21   P 0.0836 C 0.2253   (nobs 955)
RMS SV G22   P 0.0292 C 0.3718   (nobs 991)
RMS SV G23   P 0.0085 C 0.3143   (nobs 974)
RMS SV G24   P 0.0102 C 0.1430   (nobs 971)
RMS SV G26   P 0.0109 C 0.1244   (nobs 904)
RMS SV G27   P 0.0048 C 0.1132   (nobs 791)
RMS SV G28   P 0.0170 C 0.2222   (nobs 972)
RMS SV G29   P 0.0087 C 0.2454   (nobs 972)
RMS SV G30   P 0.0051 C 0.1477   (nobs 729)
RMS SV G31   P 0.0092 C 0.6597   (nobs 953)
RMS SV G32   P 0.0094 C 0.2148   (nobs 894)

RMS ALL      P 0.0212 C 0.2728   (nobs 26052)

MIN SV ELEVATION (deg) 2.23 limited to 5.00 as actual setting

OCEAN LOADING
*************
FOYL -7.336647476992448 54.983756382075782
FOYL for ocn_load OK (FOYL)
MEAN XYZ with O_LOAD applied   3638083.223 -468414.578 5200402.570
MEAN geo with O_LOAD applied   W 007°20'11.93091"  N 54°59'01.52297"  h 68.9294
O_LOAD du_min du_max  (m)      -0.0202 0.0218
MEAN HEIGHT EGM2008  68.9294 (und = 0.0000) mean traj 68.9293

FINAL REFERENCE 
***************
REF   ITRF08
PLATE EU
EPOCH 2010.0301
TRANSFORMATION DONE from ITRF05 (2010.030)  to ITRF08 (2010.030)
MEAN XYZ  ITRF08   3638083.219 -468414.576 5200402.570
MEAN geo  ITRF08   W 007°20'11.93087"  N 54°59'01.52309"  h 68.9266   (EGM08 11.278m)
MEAN prj  ITRF08   UTM_29N E  606440.104   N 6094249.364  H 11.278 EGM08
Quasi static station RMS       E 0.0889m           N 0.0739m            U 0.1022m
Error ellipse [95.00%]     semi_maj: 0.2403m (az  57.2°)  semi_min: 0.1496m (az 147.2°) U +/- 0.2003m
dE dN dU vs RNX header coo    dE +0.2556m         dN +0.3875m          dU +0.0283m

### /*adm use 3.00 0.75 0.0500 0.0800  L 0/88 */ ###
*****************************************************************
FOYL_55208.00 PPP COMPUTATION REPORT ending @ 2020 03 20 09:07:51
*****************************************************************

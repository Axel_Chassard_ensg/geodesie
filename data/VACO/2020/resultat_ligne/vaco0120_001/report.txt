*******************************************************************
VACO_58860.00 PPP INITIALISATION     starting @ 2020 03 13 10:49:19
*******************************************************************
ppp_x version 2.3 2018 08 03
quick view on (uncompressed)file vaco0120.20d
mjd_start 58860.0000  mjd_end 58860.9997 (23.9917h) sampling 30.0000s
epochs # 2880 (limitation 200000) holes in sampled data 0
(0.2852 sec)
*******************************************************************
VACO_58860.00 PPP PREPARATION REPORT starting @ 2020 03 13 10:49:19
*******************************************************************
Orbits/clocks/erp for mjd 58860.00 OK (sgu20881_00.sp3/sgu20881_00.clk_30s/sgu20881_00.erp)
Astro elements for mjd 58860.00 OK
Orbits for mjd 58860.00 successfully completed
RSW RMS (m) :   0.0038  0.0086  0.0032
SVs 31 G01 G02 G03 G05 G06 G07 G08 G09 G10 G11 G12 G13 G14 G15 G16 G17 G18 G19 G20 G21 G22 G23 G24 G25 G26 G27 G28 G29 G30 G31 G32
***warning*** large residuals for SV's : G02 G06 G11 G16 G18 G24 G08 G31 G12 G05 G32 G17 G14 G19 G27 G13 G25 G01 G28 G07 G10 G20 G30 G26 G15 G22
REFERENCE FRAME :: ITRF2008[IGS08]
(24.65 sec)
*******************************************************************
VACO_58860.00 PPP COMPUTATION REPORT starting @ 2020 03 13 10:49:44
*******************************************************************
STATION NAME   : VACO                                                        
STATION NUMBER : 11516M001                                                   
RECEIVER TYPE  : TPS NETG3           
ANTENNA TYPE   : ASH701946.2     SNOW
IGS08_1930 <ASH701946.2     SNOW>     L1 : dN  0.00170m  dE -0.00054m  dU  0.08993m
IGS08_1930 <ASH701946.2     SNOW>     L2 : dN  0.00036m  dE  0.00026m  dU  0.11974m
           <ASH701946.2     SNOW>  IF(G) : dN  0.00377m  dE -0.00178m  dU  0.04385m
           <ASH701946.2     SNOW>  IF(R) : dN  0.00375m  dE -0.00177m  dU  0.04428m
           <ASH701946.2     SNOW>  IF(E) : dN  0.00361m  dE -0.00168m  dU  0.04754m
ANTENNA HEIGHT : 0.0770
APPROX POS RNX : 4062326.022 992104.498 4800911.183  (E 013°43'27.03403"  N 49°08'01.62242"  h 799.4015)
DATE FIRST     : 2020 01 12 00 00 00.0
DATE LAST      : 2020 01 12 23 59 30.0
EPOCHS         : 2880
INTERV         : 30.000
MILLISECOND JUMPS : 2 => ignored
SV  w/o EPH    : 24  G04 R01 R02 R03 R04 R05 R06 R07 R08 R09 R10 R11 R12 R13 R14 R15 R16 R17 R18 R19 R20 R21 R22 R23 
SV  used       : 31  G01 G02 G03 G05 G06 G07 G08 G09 G10 G11 G12 G13 G14 G15 G16 G17 G18 G19 G20 G21 G22 G23 G24 G25 G26 G27 G28 G29 G30 G31 G32 
CLOCK SV ISSUES: G18  12:12:29  12:14:31
AMBIG.         : 109

APPROX SPP COMPUTATION (1ST EPOCH)
**********************************
APPROX POS SPP : 4062325.901 992105.062 4800911.835

SPP COMPUTATION (t=1.296s)
***************************
MEAN INTVL (s) : 30.000
MEAN XYZ COORD (SPP)           4062325.670 992104.829 4800911.469
MEAN geo COORD (SPP)           E 013°43'27.05402"  N 49°08'01.63491"  h 799.4453

APPROX EXTENT E x N x H m      0.5 x 1.0 x 2.5

PPP COMPUTATION (ITRF2008[IGS08] EP=2020.030)
*********************************************
ZTD relative constraint        0.010 m/h
ECLIPSE for sv G16 IIR-A   00:00:00 -> 00:48:30 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G28 IIR-A   03:57:00 -> 04:41:00 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G28 IIR-A   15:55:30 -> 16:07:00 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G12 IIR-M   19:07:00 -> 20:17:30 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G25 IIF     20:16:00 -> 21:23:30 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G26 IIF     22:54:00 -> 23:58:00 (mismodelled wind-up :: deweighted)
ECLIPSE for sv G16 IIR-A   23:34:30 -> 23:59:30 (mismodelled wind-up :: deweighted)
NOON EVENTS   : NONE
actual epochs #                2880/2880
MEAN XYZ COORD (ITRF EPOCH)    4062325.633 992104.879 4800911.382
MEAN geo COORD (ITRF EPOCH)    E 013°43'27.05685"  N 49°08'01.63366"  h 799.3641

MEAN XYZ with E_TIDES applied  4062325.644 992104.885 4800911.431
MEAN geo with E_TIDES applied  E 013°43'27.05704"  N 49°08'01.63439"  h 799.4086

PPP EXTENT E x N x H m         0.063 x 0.089 x 0.152

PPP computation time = 4.055s

RESIDUALS STATISTICS
********************
VTPV 43250.430  nobs 56582 dof:42073  sig0 1.014
RMS SV G01   P 0.0063 C 0.1095   (nobs 854)
RMS SV G02   P 0.0057 C 0.0640   (nobs 1038)
RMS SV G03   P 0.0047 C 0.0756   (nobs 815)
RMS SV G05   P 0.0048 C 0.3153   (nobs 1033)
RMS SV G06   P 0.0055 C 0.3135   (nobs 1007)
RMS SV G07   P 0.0040 C 0.0959   (nobs 1008)
RMS SV G08   P 0.0066 C 0.0491   (nobs 980)
RMS SV G09   P 0.0032 C 0.2633   (nobs 797)
RMS SV G10   P 0.0048 C 0.2123   (nobs 1031)
RMS SV G11   P 0.0083 C 0.1622   (nobs 731)
RMS SV G12   P 0.0033 C 0.3040   (nobs 809)
RMS SV G13   P 0.0032 C 0.1224   (nobs 770)
RMS SV G14   P 0.0069 C 0.1599   (nobs 976)
RMS SV G15   P 0.0040 C 0.3042   (nobs 990)
RMS SV G16   P 0.0058 C 0.2503   (nobs 900)
RMS SV G17   P 0.0066 C 0.1746   (nobs 1106)
RMS SV G18   P 0.0148 C 0.8517   (nobs 655)
RMS SV G19   P 0.0066 C 0.0690   (nobs 1035)
RMS SV G20   P 0.0050 C 0.1717   (nobs 953)
RMS SV G21   P 0.0042 C 0.0770   (nobs 925)
RMS SV G22   P 0.0042 C 0.3640   (nobs 812)
RMS SV G23   P 0.0043 C 0.2968   (nobs 823)
RMS SV G24   P 0.0029 C 0.1553   (nobs 750)
RMS SV G25   P 0.0042 C 0.0826   (nobs 807)
RMS SV G26   P 0.0058 C 0.1237   (nobs 966)
RMS SV G27   P 0.0042 C 0.1226   (nobs 899)
RMS SV G28   P 0.0040 C 0.0548   (nobs 1000)
RMS SV G29   P 0.0030 C 0.0618   (nobs 887)
RMS SV G30   P 0.0035 C 0.0605   (nobs 826)
RMS SV G31   P 0.0060 C 0.2958   (nobs 1047)
RMS SV G32   P 0.0070 C 0.0912   (nobs 1061)

RMS ALL      P 0.0057 C 0.2501   (nobs 28291)

MIN SV ELEVATION (deg) 0.15 limited to 5.00 as actual setting

OCEAN LOADING
*************
VACO 13.724182510085889 49.133787330858091
VACO for ocn_load not found!
VACO ::  ocn_loading interpolated
MEAN XYZ with O_LOAD applied   4062325.644 992104.886 4800911.431
MEAN geo with O_LOAD applied   E 013°43'27.05704"  N 49°08'01.63439"  h 799.4089
O_LOAD du_min du_max  (m)      -0.0070 0.0070
MEAN HEIGHT EGM2008  799.4089 (und = 0.0000) mean traj 799.4088

FINAL REFERENCE 
***************
REF   ITRF08
PLATE EU
EPOCH 2020.0301
TRANSFORMATION DONE from ITRF08 (2020.030)  to ITRF08 (2020.030)
MEAN XYZ  ITRF08   4062325.644 992104.886 4800911.431
MEAN geo  ITRF08   E 013°43'27.05704"  N 49°08'01.63439"  h 799.4089   (EGM08 752.375m)
MEAN prj  ITRF08   UTM_33N E  406934.778   N 5443112.096  H 752.375 EGM08
Quasi static station RMS       E 0.0098m           N 0.0099m            U 0.0240m
Error ellipse [95.00%]     semi_maj: 0.0244m (az 156.3°)  semi_min: 0.0240m (az  66.3°) U +/- 0.0469m
dE dN dU vs RNX header coo    dE +0.4664m         dN +0.3697m          dU +0.0074m

### /*adm use 3.00 0.75 0.0500 0.0800  L 0/109 */ ###
*****************************************************************
VACO_58860.00 PPP COMPUTATION REPORT ending @ 2020 03 13 10:49:53
*****************************************************************
